import { Component, Input, ElementRef, Renderer, OnChanges, SimpleChange } from '@angular/core';
import { isPresent } from 'ionic-angular/util/util';

@Component({
  selector: 'preload-image',
  templateUrl: 'preload-image.html'
})
export class PreloadImage implements OnChanges {
	_src: string = '';
	_ratio: { w: number, h: number };
  _img: any;

	constructor(public _elementRef: ElementRef, public _renderer: Renderer) {
    let env = this;
    env._img = new Image();
  }

	@Input() alt: string;

  @Input() title: string;

	@Input() set src(val: string) {
    let env = this;
    env._src = isPresent(val) ? val : '';
  }

	@Input() set ratio(ratio: { w: number, h: number }) {
		let env = this;
		env._ratio = ratio || null;
  }

  ngOnChanges(changes: { [propName: string]: SimpleChange }) {
    let env = this;
    let ratio_height = (env._ratio.h / env._ratio.w * 100) + '%';
		// Conserve aspect ratio (see: http://stackoverflow.com/a/10441480/1116959)
		env._renderer.setElementStyle(env._elementRef.nativeElement, 'padding-bottom', ratio_height);

    env._update();
    // console.log("CHANGES preload-image", this._src);
    // console.log(changes['src'].isFirstChange());
  }

	_update() {
		let env = this;
	  if (isPresent(env.alt)) {
	    env._img.alt = env.alt;
	  }
	  if (isPresent(env.title)) {
	    env._img.title = env.title;
	  }

	  env._img.addEventListener('load', function() {
      env._elementRef.nativeElement.appendChild(env._img);
			env._loaded(true);
	  });

	  env._img.src = env._src;

	  env._loaded(false);
	}

	_loaded(isLoaded: boolean) {
    let env = this;
    env._elementRef.nativeElement.classList[isLoaded ? 'add' : 'remove']('img-loaded');
  }
}
