import { Component } from '@angular/core';
import { Events, LoadingController, Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { CallNumber } from '@ionic-native/call-number';
import { DeudasProvider } from '../../providers/deudas/deudas';
import { PortalAcademicoProvider } from '../../providers/portal-academico/portal-academico';
import { GlobalVar } from '../../config';

@Component({
  selector: 'page-deudas',
  templateUrl: 'deudas.html',
})
export class DeudasPage {
  loading: any;
  deudas: any;
  str_sinConexion: string;
  web_aranceles: string;
  numeroSeleccionado: string;
  numeros: any;

  constructor(
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public events: Events,
    public deudasProvider: DeudasProvider,
    public portalAcademicoProvider: PortalAcademicoProvider,
    private dialogs: Dialogs,
    private ga: GoogleAnalytics,
    private callNumber: CallNumber,
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';
    env.web_aranceles = 'http://aranceles.uv.cl';
    env.numeros = [
      { numero: '+56322995616', texto: '+56 32 299 5616' },
      { numero: '+56322995626', texto: '+56 32 299 5626' },
      { numero: '+56322997766', texto: '+56 32 299 7766' },
      { numero: '+56322995658', texto: '+56 32 299 5658' }
    ];

    // Verificar sesión del dispositivo
    env.events.publish('verificarSesion');

    env.showCargando();
    env.deudasProvider.getDeudas(token)
      .subscribe(function(dataDeudas) {
        env.hideCargando();
        console.log(dataDeudas);
        if(dataDeudas.status == 'ERROR') {
          env.alerta(dataDeudas.error.message);
        } else if(dataDeudas.status == 'OK') {
          let objDeudas = dataDeudas.data;
          env.deudas = {
            gratuidad: objDeudas.gratuidad,
            saldoBoleta: objDeudas.saldoBoleta,
            saldoHistorico: objDeudas.saldoHistorico
          };
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-deudas');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  llamarAranceles() {
    let env = this;
    env.showCargando();
    env.callNumber.callNumber(env.numeroSeleccionado, true)
      .then(function() {
        env.hideCargando();
        console.log('Realizando llamada...');
      })
      .catch(function() {
        env.hideCargando();
        env.alerta('Error al realizar la llamada.');
      });
  }

  portalAcademico() {
    let env = this;
    let token = localStorage.getItem('token');
    env.showCargando();
    env.portalAcademicoProvider.getUrlPortal(token)
      .subscribe(function(dataPortal) {
        env.hideCargando();
        if(dataPortal.hasOwnProperty('data')) {
          window.open(dataPortal.data.url, '_system');
        } else {
          window.open(GlobalVar.PORTAL_ACADEMICO, '_system');
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  aranceles() {
    let env = this;
    window.open(env.web_aranceles, '_system');
  }
}