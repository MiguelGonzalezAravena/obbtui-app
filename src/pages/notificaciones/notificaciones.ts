import { Component } from '@angular/core';
import { Events, LoadingController, Platform} from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { NotificacionesProvider } from '../../providers/notificaciones/notificaciones';

@Component({
  selector: 'notificaciones-page',
  templateUrl: 'notificaciones.html'
})
export class NotificacionesPage {
  notificaciones: any;
  loading: any;
  str_sinConexion: string;

  constructor(
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public events: Events,
    public notificacionesProvider: NotificacionesProvider,
    private dialogs: Dialogs,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-notificaciones');
  }

  ionViewDidEnter() {
    let env = this;
    // Verificar sesión del dispositivo
    // env.events.publish('verificarSesion');
    env.cargarNotificaciones();
  }

  cargarNotificaciones() {
    let env = this;
    let token = localStorage.getItem('token');

    env.showCargando();
    env.notificacionesProvider.getNotificaciones(token)
      .subscribe(function(dataNotificaciones) {
        env.hideCargando();
        console.log(dataNotificaciones);
        if(dataNotificaciones.status == 'ERROR') {
          env.alerta(dataNotificaciones.error.message);
        } else if(dataNotificaciones.status == 'OK') {
          env.notificaciones = dataNotificaciones.data;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }
}