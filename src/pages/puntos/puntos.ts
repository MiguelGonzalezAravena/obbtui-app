import { Component } from '@angular/core';
import { Events, LoadingController, NavController, Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { PuntosProvider } from '../../providers/puntos/puntos';
import { PuntosMapaPage } from '../puntos-mapa/puntos-mapa';

@Component({
  selector: 'page-puntos',
  templateUrl: 'puntos.html',
})
export class PuntosPage {
  loading: any;
  puntos: any;
  puntosFiltrados: any;
  str_punto: string;
  str_sinConexion: string;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    private dialogs: Dialogs,
    public loadingCtrl: LoadingController,
    public events: Events,
    private ga: GoogleAnalytics,
    public puntosProvider: PuntosProvider
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';

    // Verificar sesión del dispositivo
    env.events.publish('verificarSesion');

    env.showCargando();
    env.puntosProvider.getPuntos(token)
      .subscribe(function(dataPuntos) {
        env.hideCargando();
        console.log(dataPuntos);
        if(dataPuntos.status == 'ERROR') {
          env.alerta(dataPuntos.error.message);
        } else if(dataPuntos.status == 'OK') {
          env.puntos = dataPuntos.data;
          env.puntosFiltrados = env.puntos;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-puntos');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  buscarPunto(item) {
    let env = this;
    env.puntosFiltrados = env.puntos.filter(function(elem) {
      return elem.nombre.toLowerCase().search(env.str_punto.toLowerCase()) != -1;
    });
  }

  abrirMapa(punto) {
    let env = this;
    env.navCtrl.push(PuntosMapaPage, { punto: punto });
  }

  cancelarBusqueda(item) {
    let env = this;
    env.puntosFiltrados = env.puntos;
  }
}