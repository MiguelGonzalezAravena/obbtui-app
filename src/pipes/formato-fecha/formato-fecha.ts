import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatoFecha',
})
export class FormatoFechaPipe implements PipeTransform {
  transform(value: Date): string {
    let env = this;
    let fechaMensaje = new Date(value);
    let fecha = new Date();
    let diaHoy = fecha.getUTCDate();
    let mesHoy = fecha.getUTCMonth();
    let anioHoy = fecha.getUTCFullYear();
    let diaMensaje = fechaMensaje.getUTCDate();
    let mesMensaje = fechaMensaje.getUTCMonth();
    let anioMensaje = fechaMensaje.getUTCFullYear();
    let horaMensaje = env.strDate(fechaMensaje.getUTCHours()) + ':' + env.strDate(fechaMensaje.getUTCMinutes());

    if(diaHoy == diaMensaje && mesHoy == mesMensaje && anioHoy == anioMensaje) {
      return 'Hoy, a las ' + horaMensaje;
    } else if(diaMensaje == diaHoy - 1 && mesHoy == mesMensaje && anioHoy == anioMensaje) {
      return 'Ayer, a las ' + horaMensaje;
    } else if(anioHoy == anioMensaje) {
      return env.strDate(diaMensaje) + '-' + env.strDate(mesMensaje + 1) + ', a las ' + horaMensaje;
    } else {
      return env.strDate(diaMensaje) + '-' + env.strDate(mesMensaje + 1) + '-' + anioMensaje + ', a las ' + horaMensaje;
    }
  }

  strDate(num: number): string {
    let result: string = ('0' + num).toString();
    return result.substr(-2);
  }
}